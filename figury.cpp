#include <iostream>
#include <ctime>
#include <cmath>

using namespace std;

void zatrzymaj() {
    cout << "\nKliknij cokolwiek...\n";
    cin.ignore(256, '\n');
    cin.get();
}

float * sprawdz(int count=1, bool pokazNapis=false);

void trapez();
void kolo();
void kwadrat();
void trojkat();
void wielokat();

int main()
{
    int wm = 0;
    cout << "Program do obliczen obwodow i pol figur geometrycznych\n"
         << "Wybierz jedna z opcji:\n1 - Kolo\n2 - Kwadrat\n3 - Trapez"
         << "Wpisz swoj wybor: ";
    wm=(int)sprawdz()[0];
    //cin >> wm;
//    while (cin.fail()) {
//        cout << "Podales nieprawidlowe dane. Sprobuj ponownie: ";
//        cin.clear();
//        cin.ignore(256,'\n');
//        cin >> wm;
//    }
    if (wm==3) trapez();
    else if (wm==2 || wm==1) cout << "Jeszcze nie zaimplementowano!";
    else {
        cout << "Dokonano niewlasciwego wyboru!" << endl;
    }
    zatrzymaj();
    system("cls");
    return 0;
}

void trapez() {
    float *boki;
    //cout << "Podaj 4 boki: ";
    //cin >> a >>  b >> c >> d;
//    while (cin.fail()) {
//        cout << "Podales nieprawidlowe dane. Sprobuj ponownie: ";
//        cin.clear();
//        cin.ignore(256,'\n');
//        cin >> a >> b >> c >> d;
//    }
//    cout << "Podaj wysokosc: ";
//    cin >> h;
//    while (cin.fail()) {
//        cout << "Podales nieprawidlowe dane. Sprobuj ponownie: ";
//        cin.clear();
//        cin.ignore(256,'\n');
//        cin >> h;
//    }
    //4 boki + wysokosc
    boki = sprawdz(5, true);
    boki[4] = (boki[0]+boki[1])*boki[4]/2;
    boki[0]+=boki[1]+boki[2]+boki[3];
    //boki[0]=boki[0]+boki[1]+boki[2]+boki[3]
    cout << "Obwod dla podanego trapzu wynosi: "

         << boki[0]
         << ".\nPole podanego trpezu wynosi: "
         << boki[4];

}


float * sprawdz(int count, bool pokazNapis) {
    float *zwracane = new float[count];
    for(int i=0;i<count;i++)
        zwracane[i]=.0f;
    if (pokazNapis)
        cout << "Podaj " << count << " odcinki/ow: ";
    for(int i=0;i<count;i++)
        cin >> zwracane[i];
    while (cin.fail()) {
        cout << "Podales nieprawidlowe dane. Sprobuj ponownie: ";
        cin.clear();
        cin.ignore(256,'\n');
        for(int i=0;i<count;i++)
            cin >> zwracane[i];
    }
    return zwracane;
}
